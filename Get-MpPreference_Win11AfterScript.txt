

AllowDatagramProcessingOnWinServer            : False
AllowNetworkProtectionDownLevel               : False
AllowNetworkProtectionOnWinServer             : False
AllowSwitchToAsyncInspection                  : False
AttackSurfaceReductionOnlyExclusions          : 
AttackSurfaceReductionRules_Actions           : 
AttackSurfaceReductionRules_Ids               : 
CheckForSignaturesBeforeRunningScan           : True
CloudBlockLevel                               : 4
CloudExtendedTimeout                          : 30
ComputerID                                    : 4CA42012-BF23-458D-8A9F-79DDEEE1A13D
ControlledFolderAccessAllowedApplications     : 
ControlledFolderAccessProtectedFolders        : 
DefinitionUpdatesChannel                      : 0
DisableArchiveScanning                        : False
DisableAutoExclusions                         : False
DisableBehaviorMonitoring                     : False
DisableBlockAtFirstSeen                       : False
DisableCatchupFullScan                        : True
DisableCatchupQuickScan                       : True
DisableCpuThrottleOnIdleScans                 : True
DisableDatagramProcessing                     : False
DisableDnsOverTcpParsing                      : False
DisableDnsParsing                             : False
DisableEmailScanning                          : False
DisableFtpParsing                             : False
DisableGradualRelease                         : False
DisableHttpParsing                            : False
DisableInboundConnectionFiltering             : False
DisableIOAVProtection                         : False
DisableNetworkProtectionPerfTelemetry         : False
DisablePrivacyMode                            : False
DisableRdpParsing                             : False
DisableRealtimeMonitoring                     : False
DisableRemovableDriveScanning                 : False
DisableRestorePoint                           : True
DisableScanningMappedNetworkDrivesForFullScan : True
DisableScanningNetworkFiles                   : False
DisableScriptScanning                         : False
DisableSshParsing                             : False
DisableTlsParsing                             : False
EnableControlledFolderAccess                  : 1
EnableDnsSinkhole                             : True
EnableFileHashComputation                     : False
EnableFullScanOnBatteryPower                  : False
EnableLowCpuPriority                          : False
EnableNetworkProtection                       : 0
EngineUpdatesChannel                          : 0
ExclusionExtension                            : 
ExclusionIpAddress                            : 
ExclusionPath                                 : 
ExclusionProcess                              : 
ForceUseProxyOnly                             : False
HighThreatDefaultAction                       : 0
LowThreatDefaultAction                        : 0
MAPSReporting                                 : 2
MeteredConnectionUpdates                      : False
ModerateThreatDefaultAction                   : 0
PlatformUpdatesChannel                        : 0
ProxyBypass                                   : 
ProxyPacUrl                                   : 
ProxyServer                                   : 
PUAProtection                                 : 1
QuarantinePurgeItemsAfterDelay                : 90
RandomizeScheduleTaskTimes                    : True
RealTimeScanDirection                         : 0
RemediationScheduleDay                        : 0
RemediationScheduleTime                       : 02:00:00
ReportingAdditionalActionTimeOut              : 10080
ReportingCriticalFailureTimeOut               : 10080
ReportingNonCriticalTimeOut                   : 1440
ScanAvgCPULoadFactor                          : 50
ScanOnlyIfIdleEnabled                         : True
ScanParameters                                : 1
ScanPurgeItemsAfterDelay                      : 15
ScanScheduleDay                               : 0
ScanScheduleOffset                            : 120
ScanScheduleQuickScanTime                     : 00:00:00
ScanScheduleTime                              : 02:00:00
SchedulerRandomizationTime                    : 4
ServiceHealthReportInterval                   : 60
SevereThreatDefaultAction                     : 0
SharedSignaturesPath                          : 
SignatureAuGracePeriod                        : 0
SignatureBlobFileSharesSources                : 
SignatureBlobUpdateInterval                   : 60
SignatureDefinitionUpdateFileSharesSources    : 
SignatureDisableUpdateOnStartupWithoutEngine  : False
SignatureFallbackOrder                        : MicrosoftUpdateServer|MMPC
SignatureFirstAuGracePeriod                   : 120
SignatureScheduleDay                          : 8
SignatureScheduleTime                         : 01:45:00
SignatureUpdateCatchupInterval                : 1
SignatureUpdateInterval                       : 4
SubmitSamplesConsent                          : 1
ThreatIDDefaultAction_Actions                 : 
ThreatIDDefaultAction_Ids                     : 
ThrottleForScheduledScanOnly                  : True
TrustLabelProtectionStatus                    : 0
UILockdown                                    : False
UnknownThreatDefaultAction                    : 0
PSComputerName                                : 



