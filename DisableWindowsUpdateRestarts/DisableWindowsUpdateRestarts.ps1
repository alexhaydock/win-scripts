﻿#requires -RunAsAdministrator

################################################# WARNING #################################################
#
# This Powershell script will replicate the registry settings set by the required LGPO policy objects
# that need setting in order to disable automatic reboots on Windows when Windows Updates are applied.
#
# Unfortunately, I don't think this is sufficient to stop the machine from automatically rebooting.
# When setting LGPOs with gpedit.msc there must be something else that's going on that I'm not replicating
# here.
#
# It's quite difficult though, as there are no clean options to set LGPOs via Powershell directly.
# Clearly Microsoft just want you to join the system to a domain and have that be the only way of doing
# things, but that seems excessive just to set a couple of Group Policy Objects on a local workstation.
#
################################################# WARNING #################################################

# Create the path that will hold our registry keys (it does not exist on a fresh install)
$KeyPath = "HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU"
New-Item -Path $KeyPath -Force

# The options below configure the registry keys as desired, based on the
# keys that change when we configure Windows Update using LGPO.

# When setting LGPO options via this method (direct Registry editing) it
# seems like the changes will not be reflected in gpedit.msc as it seems
# to keep some sort of local state database of LGPOs, rather than actually
# reading the current state directly from the registry.

# Never auto-reboot with logged-on users
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name NoAutoRebootWithLoggedOnUsers -Type DWord -Value 1

# 4 = Automatically download updates and install them on the schedule specified below
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name AUOptions -Type DWord -Value 4

# Not sure exactly which setting this corresponds to, but configuring the 'Configure Automatic Updates' LGPO sets this value
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name NoAutoUpdate -Type DWord -Value 0

# A value of "0" here corresponds to "Every Day"
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name ScheduledInstallDay -Type DWord -Value 0

# A value of "18" here corresponds to "Automatic", which will let Windows pick its own time to install updates
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name ScheduledInstallTime -Type DWord -Value 18

# This key configures Windows to install updates during Automatic Maintenance
Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\WindowsUpdate\AU -Name AutomaticMaintenanceEnabled -Type DWord -Value 1
