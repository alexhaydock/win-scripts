#requires -RunAsAdministrator

# This is a strong config for Microsoft Defender on an individual
# Windows 11 endpoint.

# For a rundown of these options, see:
# https://docs.microsoft.com/en-us/powershell/module/defender/set-mppreference?view=windowsserver2019-ps

# For the default settings captured from a standard Windows 11
# Pro for Workstations install, see the Get-MpPreference_Win11Defaults.txt
# file in this repo.

Write-Host "Set the cloud block level"
# Default (0): provides strong detection without increasing risk of detecting legitimate files
# Moderate (1): provides moderate detection only for high confidence detections
# High (2): applies a strong level of detection while optimizing client performance
# HighPlus (4): applies extra protection measures (might affect client performance and increase your chance of false positives)
# ZeroTolerance (6): blocks all unknown executables
#
# Default: 2
Set-MpPreference -CloudBlockLevel 4

Write-Host "Set MAPS reporting to Advanced"
# 0: Disabled - Send no information to Microsoft. This value is the default value.
# 1: Basic membership. - Send basic information to Microsoft about detected software, including where the software came from, the actions that you apply or that apply automatically, and whether the actions succeeded.
# 2: Advanced membership. - In addition to basic information, send more information to Microsoft about malicious software, spyware, and potentially unwanted software, including the location of the software, file names, how the software operates, and how it affects your computer.
#
# Default: 2
Set-MpPreference -MAPSReporting 2

Write-Host "Make sure BAFS is enabled"
#
# Default: False
Set-MpPreference -DisableBlockAtFirstSeen $False

Write-Host "Make sure real-time protection is enabled"
#
# Default: False
Set-MpPreference -DisableRealtimeMonitoring $False

Write-Host "Check for new virus definitions before running a scan"
#
# Default: False
Set-MpPreference -CheckForSignaturesBeforeRunningScan $True

Write-Host "Extend cloud scan timeout to 60 seconds"
# Specifies the amount of extended time to block a suspicious file and scan it in the cloud. Standard time is 10 seconds. Extend by up to 50 seconds.
#
# Default: 2
Set-MpPreference -CloudExtendedTimeout 30

Write-Host "Enable Controlled Folder Access"
#
# Default: 0
Set-MpPreference -EnableControlledFolderAccess 1

Write-Host "Enable behavior monitoring"
#
# Default: False
Set-MpPreference -DisableBehaviorMonitoring $False

Write-Host "Enable removable drive scanning"
#
# Default: True
Set-MpPreference -DisableRemovableDriveScanning $False

Write-Host "Scan scripts during malware scans"
#
# Default: False
Set-MpPreference -DisableScriptScanning $False

Write-Host "Enable detection of potentially unwanted apps"
# 0: Disabled
# 1: Enabled
# 2: Audit Mode
#
# Default: 2
Set-MpPreference -PUAProtection Enabled

Write-Host "Enable archive scanning"
#
# Default: False
Set-MpPreference -DisableArchiveScanning $False

Write-Host "Enable email scanning"
#
# Default: True
Set-MpPreference -DisableEmailScanning $False

Write-Host "Update definitions and signatures every 4 hours"
# 0: Check using the default interval
# All other integers: The number of hours Defender should wait between checking
#
# Default: 0
Set-MpPreference -SignatureUpdateInterval 4

Write-Host "Scan all downloaded files and attachments"
#
# Default: False
Set-MpPreference -DisableIOAVProtection $False

Write-Host "Enable safe sample submission"
# 0: Always prompt
# 1: Send safe samples automatically
# 2: Never send
# 3: Send all samples automatically
#
# Default: 1
Set-MpPreference -SubmitSamplesConsent 1
