# win-scripts

Scripts and other config for Windows 10 and Windows 11.

Everything in the root directory of this repository should work the same on both Win 10 and Win 11. Anything in the `win10/` or `win11/` directories is something that is likely version specific, or is simply something that I haven't tested on both systems.

## Prerequisites

### Allow PowerShell script execution
1. Run PowerShell as Administrator
2. Allow running unsigned local (but not remote) scripts with: `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned`
3. We can verify this worked with `Get-ExecutionPolicy -List`
4. If the scripts in the current directory still refuse to run as Windows has tagged them as being downloaded from the internet, try `Unblock-File *.ps1`
