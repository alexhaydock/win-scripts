#requires -RunAsAdministrator

# To get the list of startup programs for the current user:
## Get-ItemProperty HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
## Get-ItemProperty HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\RunOnce

# This seems to work but is reasonably unreliable due to the large number
# of programs (including some of MS's own like Power Automate) which must be
# scheduling their run-on-startup somewhere other than these registry keys.

Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run -Name 'Discord' -Value ([byte[]](0x33,0x32,0xFF))
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run -Name 'OneDrive' -Value ([byte[]](0x33,0x32,0xFF))
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run -Name 'org.whispersystems.signal-desktop' -Value ([byte[]](0x33,0x32,0xFF))
