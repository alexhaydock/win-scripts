#requires -RunAsAdministrator

# This is an auto-install script that leverages the
# WinGet package manager built into Windows 10 to
# install a user-defined list of apps.
#
# It will not install apps already installed via
# WinGet so it is safe to run as many times as desired.

# Define list of apps here (we can discover these with a simple `winget search appname`)
#
# MS Store Apps:
#   - XP9KHM4BK9FZ7Q = Visual Studio Code
#   - XPDC2RH70K22MN = Discord
#   - XPDM27W10192Q0 = GIMP
#
# Bitwarden is installed directly rather than via the MS Store as it does not allow itself
# to be installed without a signed-in store account. I have had the same problem previously
# with the Spotify app in the MS Store.
#
$apps = @(
    @{name = "7zip.7zip" },
    @{name = "Axosoft.GitKraken" },
    @{name = "Balena.Etcher" },
    @{name = "Bitwarden.Bitwarden" },
    @{name = "Obsidian.Obsidian" },
    @{name = "OpenWhisperSystems.Signal" },
    @{name = "RaspberryPiFoundation.RaspberryPiImager" },
    @{name = "TheDocumentFoundation.LibreOffice" },
    @{name = "Transmission.Transmission" },
    @{name = "WinSCP.WinSCP" },
    @{name = "WireGuard.WireGuard" },
    @{name = "WiresharkFoundation.Wireshark" },
    @{name = "XP9KHM4BK9FZ7Q" },
    @{name = "XPDC2RH70K22MN" },
    @{name = "XPDM27W10192Q0" }
);

# Install the selected apps we defined above
Foreach ($app in $apps) {
    # Check if the app is already installed
    # so we can skip installing it if it is
    #
    # (WinGet doesn't seem to be smart enough
    # to be idempotent without a little help)

    if (winget list --exact -q $app.name | Select-String -SimpleMatch $app.name) {
        Write-Host $app.name "is installed. Skipping."
    } else {
        Write-Host $app.name "is not installed. Installing now."
        winget install --exact --silent $app.name --accept-package-agreements --accept-source-agreements
    }
}
